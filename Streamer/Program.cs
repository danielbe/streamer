﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Streamer
{
    class Program
    {
        static void Main(string[] args)
        {
            //initialize settings
            List<string> settings = FileStuff.ReadAllLines("settings.txt").ToList();
            Values.CurrentQuality = settings.Find(s => s == "quality") ?? Values.DefaultQuality;
            Values.CurrentMpvParams = settings.Find(s => s == "mpvParams") ?? Values.DefaultMpvParams;
            Values.CurrentPlayer = settings.Find(s => s == "defaultPlayer") ?? Values.DefaultPlayer;
            Values.CurrentFilenameParams = settings.Find(s => s == "streamlinkParams") ?? Values.DefaultFilenameParams;

            Console.WriteLine("Type the stream to be started, \"q\" to exit, or \"h\" to display help");
            while (true)
            {
                string userInput = Console.ReadLine();
                if ("q" == userInput)
                {
                    Environment.Exit(0);
                }
                else if ("h" == userInput)
                {
                    Console.WriteLine(Values.HelpText);
                }
                else if (userInput.StartsWith("set "))
                {
                    string msg = UserInput.Settings(userInput);
                    if (msg != null) Console.WriteLine(msg);
                }
                else if (userInput.StartsWith("kill "))
                {
                    Stream found = UserInput.Find(userInput.Split(" ")[1]);
                    Console.WriteLine("found this: " + found);
                }
                else
                {
                    UserInput.Evaluate(userInput);
                }
            }
        }
    }
}
