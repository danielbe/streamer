﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Channels;

namespace Streamer
{
    static class Values
    {
        static internal readonly string DefaultQuality = "480p,540p,best";
        static internal readonly string DefaultPlatform = "twitch.tv/";
        internal const string DefaultMpvParams = "--no-border --mute --screen=1";
        internal const string DefaultFilenameParams = "--player mpv --player-args";
        internal const string DefaultPlayer = "mpv";
        static internal Mode CurrentMode { get; set; } = Mode.Normal;

        static internal string CurrentQuality { get; set; } = DefaultQuality;
        static internal string CurrentPlatform { get; set; } = DefaultPlatform;
        static internal string CurrentMpvParams { get; set; } = DefaultMpvParams;
        static internal string CurrentFilenameParams { get; set; } = DefaultFilenameParams;
        static internal string[] CurrentStreams { get; set; }
        static internal Stream[] CurrentProcesses { get; set; }
        static internal string CurrentPlayer { get; set; }

        static internal readonly string HelpText = "Needs programs streamlink, mpv and ffmpeg" + 
            "//examples to start stream(s): " + Environment.NewLine +
            "//simply open stream" + Environment.NewLine +
            "twitchplayspokemon" + Environment.NewLine +
            "//open in bottom left corner (with 4 stream mode active)" + Environment.NewLine +
            "twitchplayspokemon 3" + Environment.NewLine +
            "//open 4 streams" + Environment.NewLine +
            "twitchplayspokemon channel2 channel3 channel4" + Environment.NewLine +
            Environment.NewLine +
            "//examples to change settings:" + Environment.NewLine +
            "set quality 720p" + Environment.NewLine +
            "//720p as a fallback option" + Environment.NewLine +
            "set quality 720p60,720p" + Environment.NewLine +
            "//switch to 9 stream mode" + Environment.NewLine +
            "set mode 9" + Environment.NewLine +
            Environment.NewLine +
            "//static URLs may be put in text file urls.txt in same folder" + Environment.NewLine +
            ".servustv" + Environment.NewLine +
            "//File urls.txt must contain the follwing:" + Environment.NewLine +
            "servustv https://urlofservustv.m3u8";

        static internal readonly string AltQuality = "best";

        static internal string GetPositionOfMode(int position)
        {
            string[] positionMode = CurrentMode switch
            {
                Mode.Four => positionModeFour,
                Mode.Nine => positionModeNine,
                Mode.ThreeOne => positionModeFour, //
                _ => positionModeFour
            };

            return positionMode.Length > position
                ? positionMode[position]
                : "";
        }

        static internal readonly string[] positionModeFour = { "", "--geometry=50%+0%+0%", "--geometry=50%+100%+0%", "--geometry=50%+0%+100%", "--geometry=50%+100%+100%" };
        static internal readonly string[] positionModeNine = {
            "",
            "--geometry=34%+0%+0%",
            "--geometry=34%+50%+0%",
            "--geometry=34%+100%+0%",
            "--geometry=34%+0%+50%",
            "--geometry=34%+50%+50%",
            "--geometry=34%+100%+50%",
            "--geometry=34%+0%+100%",
            "--geometry=34%+50%+100%",
            "--geometry=34%+100%+100%"
        };


        internal enum Mode
        {
            Normal,
            Four,
            Nine,
            ThreeOne
        }

        /*static void Test()
        {
            System.Windows.Forms
        }*/
    }
}
