﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Streamer
{
    internal class Stream
    {
        internal string Platform { get; set; } //includes full url if not twitch
        internal string Channel { get; set; } //twitch channel name
        internal string Quality { get; set; }
        internal string MpvParams { get; set; }
        internal string Filename { get; set; } = "streamlink";
        internal string Position { get; set; }
        internal string FilenameParams { get; set; }
        int I = 0;

        internal Process process;

        internal Stream() { }

        internal void Start(bool useMpv)
        {
            //Console.WriteLine($"{platform}{channel} {quality} --player \"{mpvParams}\"");
            using (process = new Process())
            {
                string arguments = useMpv 
                    ? $"{Platform} {MpvParams} {Position}"
                    : $"{Platform}{Channel} {Quality} {FilenameParams} \"{MpvParams} {Position}\"";
                process.StartInfo = new ProcessStartInfo()
                {
                    //FileName = "notepad",
                    FileName = Filename,
                    Arguments = arguments,
                    //Arguments = $"{platform}{channel} {quality}",
                    CreateNoWindow = false
                };
                /*
                    Process process = new Process
                {
                    StartInfo =
                    {
                      FileName = "streamlink",
                      Arguments = $"{Platform}{Channel} {Quality} --player \"{MpvParams}\"",
                      CreateNoWindow = false
                    }
                };*/
                process.Exited += new EventHandler(Test);
                process.Disposed += new EventHandler(Test);
                process.EnableRaisingEvents = true;
                process.Start();
                //process.WaitForExit();
            //process.OutputDataReceived += new DataReceivedEventHandler(Test);
            }
        }

        internal void Kill()
        {
            process.Kill();
        }

        internal void Test(object sender, EventArgs e)
        {
            Console.WriteLine("fsedfesfef");
        }
    }
}
