﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Streamer
{
    class FileStuff
    {
        internal static List<string> ReadAllLines(string file)
        {
            List<string> ret = new List<string>();

            try
            {
                ret = File.ReadLines(file).ToList();
            }
            catch(Exception)
            {
                Console.WriteLine($"Datei {file} kann nicht ausgelesen werden!");
            }

            return ret;
        }
    }
}
