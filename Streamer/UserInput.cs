﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Enumeration;
using System.Linq;
using System.Text;

namespace Streamer
{
    class UserInput
    {
        public delegate void SampleEventHandler(object sender, Stream e);
        public event SampleEventHandler SampleEvent;

        internal static void Evaluate()
        {
            int inputsLength = Values.CurrentStreams.Length;
            if (inputsLength == 1) Execute(Values.CurrentStreams[0], Values.DefaultPlatform, Values.CurrentQuality);
            else if (inputsLength == 2)
            {
                if (int.TryParse(Values.CurrentStreams[1], out int result))
                    //callExecute(Values.CurrentStreams);
                    Execute(Values.CurrentStreams[0], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams, Values.GetPositionOfMode(result));
                else
                    Execute(Values.CurrentStreams[0], Values.DefaultPlatform, Values.CurrentStreams[1]);
            }
            else if (inputsLength == 3) Execute(Values.CurrentStreams[1], Values.CurrentStreams[0], Values.CurrentStreams[2]); //a bit pointless atm
            else if (inputsLength == 4)
            {
                Values.CurrentMode = Values.Mode.Four;
                callExecute(Values.CurrentStreams);
                //Execute(inputs[0], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeFour[1]);
                //Execute(inputs[1], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeFour[2]);
                //Execute(inputs[2], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeFour[3]);
                //Execute(inputs[3], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeFour[4]);
                //Array.ForEach(inputs, input => Execute(input, Values.DefaultPlatform, Values.DefaultQuality));
            }
            else if (inputsLength == 9)
            {
                Values.CurrentMode = Values.Mode.Nine;
                callExecute(Values.CurrentStreams);
                //Execute(inputs[0], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[1]);
                //Execute(inputs[1], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[2]);
                //Execute(inputs[2], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[3]);
                //Execute(inputs[3], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[4]);
                //Execute(inputs[4], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[5]);
                //Execute(inputs[5], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[6]);
                //Execute(inputs[6], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[7]);
                //Execute(inputs[7], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[8]);
                //Execute(inputs[8], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams + " " + Values.positionModeNine[9]);
            }
        }

        internal static void Evaluate(string userInput)
        {
            Values.CurrentStreams = userInput.Trim().Split(" ");
            Evaluate();
        }

        static void callExecute(string[] inputs)
        {
            for(int i = 0; i < inputs.Length; i++)
            {
                Execute(inputs[i], Values.DefaultPlatform, Values.CurrentQuality, Values.CurrentMpvParams, Values.GetPositionOfMode(i+1), i);
            }
        }

        static Stream Execute(string channel, string platform, string quality, string mpvParams = Values.DefaultMpvParams, string position = "", int i = -1)
        {
            Stream stream = null;
            string filename = "";
            bool useMpv = false;

            if (channel.EndsWith(".m3u8"))
            {
                platform = channel;
                channel = "";
                quality = "";
                useMpv = true;
            }
            else if (channel.StartsWith("http://") || channel.StartsWith("https://"))
            {
                platform = channel;
                channel = "";
            }
            else if (channel.StartsWith(".")) //check file
            {
                List<string> urls = FileStuff.ReadAllLines("urls.txt").ToList();
                channel = channel.Substring(1);
                string found = urls.Find(u => u.StartsWith(channel)) ?? "";
                platform = found.Contains(" ")
                    ? found.Split(" ")[1]
                    : null;
                channel = "";

                //https://stvlvstrm.redbullmediahouse.com/hls/live/2031011/lingeoSTVATwebPri/master.m3u8
            }

            filename = useMpv ? Values.CurrentPlayer : "streamlink";
            stream = new Stream() { Channel = channel, Platform = platform, Quality = quality, MpvParams = mpvParams, 
                Filename = filename, FilenameParams = Values.DefaultFilenameParams, Position = position };
            //if(i != -1)
            //    Values.CurrentProcesses[i] = stream;
            stream.Start(useMpv);

            return stream;
        }

        internal static Stream Find(string name)
        {
            int size = Values.CurrentProcesses.Length;
            Stream found = null;

            if(int.TryParse(name, out int nr))
                found = (nr - 1 < size) ? Values.CurrentProcesses[nr] : null;
            else
            {
                found = Values.CurrentProcesses.Where(s => s.Channel == name).First();
            }

            return found;
        }

        static void Test(object sender, EventHandler e)
        {

        }

        internal static string Settings(string userInput)
        {
            string[] inputs = userInput.Trim().Split(" ");
            int inputsLength = inputs.Length;
            string returnMsg = inputs[1] switch
            {
                "quality" => setQuality(inputs),
                "mode" => setMode(inputs),
                "mpv" => setMpvParams(inputs, userInput),
                _ => null
            };

            return returnMsg;
        }

        static string setQuality(string[] inputs)
        {
            bool set = inputs.Length > 2;
            Values.CurrentQuality = set ? inputs[2] : Values.CurrentQuality;
            return $"quality {(set ? "was set to" : "currently is")} {Values.CurrentQuality}";
        }

        static string setMpvParams(string[] inputs, string input)
        {
            bool set = inputs.Length > 2;
            Values.CurrentMpvParams = set ? (input.Replace("set mpv", "").Trim()) : Values.CurrentMpvParams;
            return $"mpv parameters {(set ? "were set to" : "currently are")} {Values.CurrentMpvParams}";
        }

        internal static string setMode(string[] inputs)
        {
            bool set = inputs.Length > 2;
            if(set)
            {
                Values.CurrentMode = inputs[inputs.Length - 1] switch
                {
                    "9" => Values.Mode.Nine,
                    "4" => Values.Mode.Four,
                    "13" => Values.Mode.ThreeOne,
                    _ => Values.CurrentMode
                };
            }
            Evaluate();

            return $"mode {(set ? "was set to" : "currently is")} {Values.CurrentMode}";
        }

    }
}
